package id.ac.ui.cs.advprog.tutorial3.decorator;

import id.ac.ui.cs.advprog.tutorial3.decorator.bread.CrustySandwich;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.BeefMeat;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.Cheese;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.Lettuce;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.TomatoSauce;

public class MainFood {
    public static void main(String[] args) {
        Food food = new CrustySandwich();

        System.out.println(food.getDescription() + "\nTotal : "  + food.cost());

        food = new BeefMeat(food);
        food = new Cheese(food);
        food = new Lettuce(food);
        food = new TomatoSauce(food);
        System.out.println(food.getDescription() + "\nTotal : " + food.cost());
    }
}
