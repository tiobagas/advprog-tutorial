package id.ac.ui.cs.advprog.tutorial3.composite;

import id.ac.ui.cs.advprog.tutorial3.composite.higherups.Ceo;
import id.ac.ui.cs.advprog.tutorial3.composite.higherups.Cto;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.*;

public class MainCompany {
    public static void main(String[] args) {
        Employees ceo = new Ceo("Tio", 250000.00);
        Employees cto = new Cto("Bagas", 150000.00);
        Employees backendProgrammer = new BackendProgrammer("Sulistiyanto", 25000.0);
        Employees frontendProgrammer = new FrontendProgrammer("Daus", 35000.00);
        Employees networkExpert = new NetworkExpert("Adib", 55000.00);
        Employees securityExpert = new SecurityExpert("Zain", 75000.00);
        Employees uiuxDesigner = new UiUxDesigner("Thoriq", 95000.00);

        Company company = new Company();

        company.addEmployee(ceo);
        company.addEmployee(cto);
        company.addEmployee(backendProgrammer);
        company.addEmployee(frontendProgrammer);
        company.addEmployee(networkExpert);
        company.addEmployee(securityExpert);
        company.addEmployee(uiuxDesigner);

        System.out.println(company.getNetSalaries());

        for (Employees employees: company.getAllEmployees()) {
            System.out.println(employees.getName());
        }
    }
}
