import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

public class CustomerTest {

    // TODO: Remove redundancy in setting up test fixture in each test methods
    // Hint: Make the test fixture into an instance variable
    Customer customer;
    Movie movie;
    Rental rent;


    @Before
    public void setUp() throws Exception {
        customer = new Customer("Alice");
        movie = new Movie("Who Killed Captain Alex?", Movie.REGULAR);
        rent = new Rental(movie, 3);
    }

    @Test
    public void getName() {
        assertEquals("Alice", customer.getName());
    }

    @Test
    public void statementWithSingleMovie() {
        customer.addRental(rent);

        String result = customer.statement();
        String[] lines = result.split("\n");

        assertEquals(4, lines.length);
        assertTrue(result.contains("Amount owed is 3.5"));
        assertTrue(result.contains("1 frequent renter points"));
    }

    // TODO Implement me!
    @Test
    public void statementWithMultipleMovies() {
        // TODO Implement me!
        movie = new Movie("Avenger", Movie.REGULAR);
        rent = new Rental(movie, 4);
        customer.addRental(rent);

        movie = new Movie("Black Panther", Movie.NEW_RELEASE);
        rent = new Rental(movie, 5);
        customer.addRental(rent);

        String result = customer.statement();
        String[] lines = result.split("\n");

        assertEquals(5, lines.length);
        assertTrue(result.contains("Amount owed is 20.0"));
        assertTrue(result.contains("3 frequent renter points"));
    }

    @Test
    public void htmlStatement() {
        customer.addRental(rent);
        String result = customer.htmlStatement();

        assertTrue(result.contains("Amount owed is <em>3.5<em>"));
        assertTrue(result.contains("On this rental you earned <em>1"));
    }
}