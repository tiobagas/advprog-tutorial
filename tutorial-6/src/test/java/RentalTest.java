import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class RentalTest {

    // TODO: Remove redundancy in setting up test fixture in each test methods
    // Hint: Make the test fixture into an instance variable
    Movie movie;
    Movie movieChildren;
    Rental rent;
    Rental rentChildren;

    @Before
    public void setUp() throws Exception {
        movie = new Movie("Who Killed Captain Alex?", Movie.REGULAR);
        movieChildren = new Movie("Who Killed Captain Alex?", Movie.CHILDREN);
        rent = new Rental(movie, 3);
        rentChildren = new Rental(movieChildren, 4);
    }

    @Test
    public void getMovie() {
        assertEquals(movie, rent.getMovie());
    }

    @Test
    public void getDaysRented() {
        assertEquals(3, rent.getDaysRented());
    }

    @Test
    public void getThisAmount() {
        assertEquals(rentChildren.getThisAmount(), 3.0, 10.0);
    }

    @Test
    public void getFrequentRenterPoints() {
        assertEquals(rentChildren.getFrequentRenterPoints(), 1);
    }
}