import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

public class MovieTest {

    // TODO: Remove redundancy in setting up test fixture in each test methods
    // Hint: Make the test fixture into an instance variable
    Movie movie1;
    Movie movie2;

    @Before
    public void setUp() throws Exception {
        movie1 = new Movie("Who Killed Captain Alex?", Movie.REGULAR);
        movie2 = new Movie("Who Killed Captain Alex?", Movie.REGULAR);
    }

    @Test
    public void getTitle() {
        assertEquals("Who Killed Captain Alex?", movie1.getTitle());
    }

    @Test
    public void setTitle() {
        movie1.setTitle("Bad Black");

        assertEquals("Bad Black", movie1.getTitle());
    }

    @Test
    public void getPriceCode() {
        assertEquals(Movie.REGULAR, movie1.getPriceCode());
    }

    @Test
    public void setPriceCode() {
        movie1.setPriceCode(Movie.CHILDREN);

        assertEquals(Movie.CHILDREN, movie1.getPriceCode());
    }

    @Test
    public void equals() {
        Object o = new Object();
        assertEquals(movie1, movie2);
        assertFalse(movie1.equals(o));
        assertTrue(movie1.equals(movie2));
    }

    @Test
    public void testHashCode() {
        assertTrue(movie1.hashCode() == movie2.hashCode());
    }
}