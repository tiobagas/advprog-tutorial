import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

class Customer {

    private String name;
    private List<Rental> rentals = new ArrayList<>();

    public Customer(String name) {
        this.name = name;
    }

    public void addRental(Rental arg) {
        rentals.add(arg);
    }

    public String getName() {
        return name;
    }

    public String statement() {
        Iterator<Rental> iterator = rentals.iterator();
        String result = "Rental Record for " + getName() + "\n";

        while (iterator.hasNext()) {
            Rental each = iterator.next();
            result += "\t" + each.getMovie().getTitle() + "\t" +
                    String.valueOf(each.getThisAmount()) + "\n";
        }

        result += "Amount owed is " + String.valueOf(getTotalAmount()) + "\n";
        result += "You earned " + String.valueOf(getTotalFrequentRenterPoints()) + " frequent renter points";

        return result;
    }

    public double getTotalAmount() {
        double totalAmount = 0;
        Iterator<Rental> iterator = rentals.iterator();
        while (iterator.hasNext()) {
            Rental each = iterator.next();
            totalAmount += each.getThisAmount();
        }
        return totalAmount;
    }

    public int getTotalFrequentRenterPoints() {
        int frequentRenterPoints = 0;
        Iterator<Rental> iterator = rentals.iterator();
        while (iterator.hasNext()) {
            Rental each = iterator.next();
            frequentRenterPoints += each.getFrequentRenterPoints();
        }
        return frequentRenterPoints;
    }

    public String htmlStatement() {
        Iterator<Rental> iterator = rentals.iterator();
        String result = "<h1>Rentals for <em> " + getName() + "<em></h1><p>\n";
        while (iterator.hasNext()) {
            Rental each = iterator.next();

            result += each.getMovie().getTitle() + ": "
                    + String.valueOf(each.getThisAmount())
                    + "<br>\n";
        }
        result += "<p>Amount owed is <em>" + String.valueOf(getTotalAmount())
                + "<em><p>\n";
        result += "On this rental you earned <em>"
                + String.valueOf(getTotalFrequentRenterPoints())
                + "<em> frequent renter points<p>";
        return result;
    }
}