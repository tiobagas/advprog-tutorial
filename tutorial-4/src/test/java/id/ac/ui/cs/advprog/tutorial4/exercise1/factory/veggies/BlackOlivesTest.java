package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class BlackOlivesTest {
    private BlackOlives blackOlives;

    @Before
    public void setUp() throws Exception {
        blackOlives = new BlackOlives();
    }

    @Test
    public void testToString() {
        assertEquals("Black Olives", blackOlives.toString());
    }
}
