package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CeleryTest {
    private Celery celery;

    @Before
    public void setUp() throws Exception {
        celery = new Celery();
    }

    @Test
    public void testToString() {
        assertEquals("Celery", celery.toString());
    }
}
