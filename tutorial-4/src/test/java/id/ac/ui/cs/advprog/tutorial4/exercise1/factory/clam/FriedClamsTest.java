package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class FriedClamsTest {
    private FriedClams friedClams;

    @Before
    public void setUp() throws Exception {
        friedClams = new FriedClams();
    }

    @Test
    public void testToString() {
        assertEquals("Fried Clams", friedClams.toString());
    }
}
