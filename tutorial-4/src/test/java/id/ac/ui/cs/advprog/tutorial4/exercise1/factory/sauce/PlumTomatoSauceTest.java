package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class PlumTomatoSauceTest {
    private PlumTomatoSauce plumTomatoSauce;

    @Before
    public void setUp() throws Exception {
        plumTomatoSauce = new PlumTomatoSauce();
    }

    @Test
    public void testToString() {
        assertEquals("Tomato sauce with plum tomatoes", plumTomatoSauce.toString());
    }
}
