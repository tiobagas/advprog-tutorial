package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class FrozenClamsTest {
    private FrozenClams frozenClams;

    @Before
    public void setUp() throws Exception {
        frozenClams = new FrozenClams();
    }

    @Test
    public void testToString() {
        assertEquals("Frozen Clams from Chesapeake Bay", frozenClams.toString());
    }
}
