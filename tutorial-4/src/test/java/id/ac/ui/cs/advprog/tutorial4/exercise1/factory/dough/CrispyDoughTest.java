package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CrispyDoughTest {
    private CrispyDough crispyDough;

    @Before
    public void setUp() throws Exception {
        crispyDough = new CrispyDough();
    }

    @Test
    public void testToString() {
        assertEquals("Crispy Dough", crispyDough.toString());
    }
}
