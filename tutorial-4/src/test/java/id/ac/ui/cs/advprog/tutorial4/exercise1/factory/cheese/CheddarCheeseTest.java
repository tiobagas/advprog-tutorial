package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.CheddarCheese;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CheddarCheeseTest {
    private CheddarCheese cheddarCheese;

    @Before
    public void setUp() throws Exception {
        cheddarCheese = new CheddarCheese();
    }

    @Test
    public void testToString() {
        assertEquals("Cheddar Cheese", cheddarCheese.toString());
    }
}
