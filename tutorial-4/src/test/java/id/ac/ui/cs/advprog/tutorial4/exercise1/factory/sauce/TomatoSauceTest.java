package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TomatoSauceTest {
    private TomatoSauce tomatoSauce;

    @Before
    public void setUp() throws Exception {
        tomatoSauce = new TomatoSauce();
    }

    @Test
    public void testToString() {
        assertEquals("Tomato Sauce", tomatoSauce.toString());
    }
}
