package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;

public class FriedClams implements Clams {
    @Override
    public String toString() {
        return "Fried Clams";
    }
}
