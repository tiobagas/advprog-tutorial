package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough;

public class CrispyDough implements Dough {
    @Override
    public String toString() {
        return "Crispy Dough";
    }
}
